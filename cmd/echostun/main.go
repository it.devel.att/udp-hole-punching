package main

import (
	"bytes"
	"encoding/binary"
	"filesharetorrent/pkg/constants"
	"filesharetorrent/pkg/utils"
	"fmt"
	"net"
	"strings"
	"sync"
)

type StunServer struct {
	udpConn *net.UDPConn

	mu    sync.RWMutex
	peers map[string]struct{}
}

func main() {
	localAddr, err := net.ResolveUDPAddr("udp", ":9898")
	if err != nil {
		panic(err)
	}
	udpConn, err := net.ListenUDP("udp", localAddr)
	if err != nil {
		panic(err)
	}

	stunServer := &StunServer{
		udpConn: udpConn,
		peers:   make(map[string]struct{}),
	}
	stunServer.Start()
}

func (s *StunServer) Start() {
	s.mainLoop()
}

func (s *StunServer) mainLoop() {
	// messageBuff := make([]byte, 1024)
	commandBuff := make([]byte, 8)
	// messageLenBuff := make([]byte, 8)
	for {
		size, remoteAddr, err := s.udpConn.ReadFrom(commandBuff)
		if err != nil {
			fmt.Println("udpConn.ReadFrom err: ", err)
			continue
		}
		if size != len(commandBuff) {
			fmt.Println("message always should start with command bytes")
			continue
		}

		var commandOP uint64
		err = binary.Read(bytes.NewBuffer(commandBuff), binary.BigEndian, &commandOP)
		if err != nil {
			fmt.Printf("Error converting first 8 bytes to buffer: %v\n", err)
			continue
		}

		switch commandOP {
		case constants.PING:
			message := utils.CreateMessageBytes(constants.PING, []byte{})
			s.udpConn.WriteTo(message, remoteAddr)
		case constants.REGISTER:
			s.mu.Lock()
			s.peers[remoteAddr.String()] = struct{}{}
			fmt.Println("Peers: ", len(s.peers))
			s.mu.Unlock()
		case constants.GET_PEERS:
			peers := s.Peers(remoteAddr.String())

			peersStr := strings.Join(peers, ",")
			message := utils.CreateMessageBytes(constants.GET_PEERS, []byte(peersStr))
			s.udpConn.WriteTo(message, remoteAddr)
		}
	}
}

func (s *StunServer) Peers(exclude string) []string {
	s.mu.RLock()
	defer s.mu.RUnlock()

	peers := make([]string, 0, len(s.peers))
	for peer, _ := range s.peers {
		if peer == exclude {
			continue
		}
		peers = append(peers, peer)
	}
	return peers
}

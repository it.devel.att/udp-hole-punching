package main

import (
	"bytes"
	"encoding/binary"
	"filesharetorrent/pkg/constants"
	"filesharetorrent/pkg/utils"
	"flag"
	"fmt"
	"net"
	"strings"
	"sync"
	"time"
)

type Client struct {
	stunServerAddr *net.UDPAddr
	udpConn        *net.UDPConn

	mu    sync.RWMutex
	peers map[string]struct{}
}

func main() {
	stunServerPtr := flag.String("stun-server", "", "ip:port of stun server")
	localPortPtr := flag.String("local-port", "", "port of local udp server")
	flag.Parse()

	stunServer := *stunServerPtr
	localPort := *localPortPtr
	var laddr *net.UDPAddr
	if localPort != "" {
		var err error
		laddr, err = net.ResolveUDPAddr("udp", localPort)
		if err != nil {
			panic(err)
		}
	}

	remoteAddr, err := net.ResolveUDPAddr("udp", stunServer)
	if err != nil {
		panic(err)
	}
	fmt.Println(remoteAddr.String())

	udpConn, err := net.ListenUDP("udp", laddr)
	if err != nil {
		panic(err)
	}
	fmt.Println("LocalAddr: ", udpConn.LocalAddr().String())

	client := &Client{
		stunServerAddr: remoteAddr,
		udpConn:        udpConn,
		peers:          map[string]struct{}{},
	}
	client.Start()
}

func (c *Client) Start() {
	go c.pingLoop()
	go c.registerLoop()
	go c.getPeersLoop()
	go c.helloPeers()

	c.receiveFromServerLoop()
}

func (c *Client) receiveFromServerLoop() {
	messageBuffer := make([]byte, 1024)
	for {
		size, resolveAddr, err := c.udpConn.ReadFrom(messageBuffer)
		if err != nil {
			fmt.Println("c.udpConn.ReadFrom err: ", err)
			continue
		}
		// if size != len(commandBuff) {
		// 	fmt.Println("Readed size not equal to command OP size: ", size)
		// 	continue
		// }
		message := messageBuffer[:size]
		// 8 bytes of command + 8 bytes of message len
		if len(message) < 16 {
			continue
		}

		commandBuff := message[:8]

		var commandOP uint64
		err = binary.Read(bytes.NewBuffer(commandBuff), binary.BigEndian, &commandOP)
		if err != nil {
			fmt.Printf("Error converting first 8 bytes to buffer: %v\n", err)
			continue
		}

		switch commandOP {
		case constants.GET_PEERS:
			messageSizeBuff := message[8:16]

			messageSize := numberFromBytes(messageSizeBuff)
			if messageSize <= 0 {
				// fmt.Println("Message size problem", messageSize)
				continue
			}
			messageBuff := message[16 : 16+messageSize]

			peersStr := string(messageBuff)
			peers := strings.Split(peersStr, ",")

			newPeers := make(map[string]struct{}, len(peers))
			for _, p := range peers {
				newPeers[p] = struct{}{}
			}

			c.mu.Lock()
			c.peers = newPeers
			c.mu.Unlock()
		case constants.PEER_MESSAGE:
			messageSizeBuff := message[8:16]

			messageSize := numberFromBytes(messageSizeBuff)
			if messageSize <= 0 {
				// fmt.Println("Message size problem", messageSize)
				continue
			}
			messageBuff := message[16 : 16+messageSize]

			message := string(messageBuff)

			fmt.Printf("[Peer:%v] %v\n", resolveAddr.String(), message)
		}
	}
}

func numberFromBytes(buff []byte) uint64 {
	var commandOP uint64
	err := binary.Read(bytes.NewBuffer(buff), binary.BigEndian, &commandOP)
	if err != nil {
		fmt.Printf("Error converting first 8 bytes to buffer: %v\n", err)

	}
	return commandOP
}

func (c *Client) pingLoop() {
	timer := time.NewTicker(time.Second * 3)
	defer timer.Stop()

	for {
		select {
		case <-timer.C:
			message := utils.CreateMessageBytes(constants.PING, []byte{})
			_, err := c.udpConn.WriteTo(message, c.stunServerAddr)
			if err != nil {
				fmt.Println("[pingLoop.WriteTo] Err: ", err)
			}
		}
	}
}

func (c *Client) registerLoop() {
	timer := time.NewTicker(time.Minute)
	defer timer.Stop()

	message := utils.CreateMessageBytes(constants.REGISTER, []byte{})
	_, err := c.udpConn.WriteTo(message, c.stunServerAddr)
	if err != nil {
		fmt.Println("[registerLoop.WriteTo] Err: ", err)
	}

	for {
		select {
		case <-timer.C:
			message := utils.CreateMessageBytes(constants.REGISTER, []byte{})
			_, err := c.udpConn.WriteTo(message, c.stunServerAddr)
			if err != nil {
				fmt.Println("[registerLoop.WriteTo] Err: ", err)
			}
		}
	}
}

func (c *Client) getPeersLoop() {
	timer := time.NewTicker(time.Second * 5)
	defer timer.Stop()

	for {
		select {
		case <-timer.C:
			message := utils.CreateMessageBytes(constants.GET_PEERS, []byte{})
			_, err := c.udpConn.WriteTo(message, c.stunServerAddr)
			if err != nil {
				fmt.Println("[getPeersLoop.WriteTo] Err: ", err)
			}
		}
	}
}

func (c *Client) helloPeers() {
	timer := time.NewTicker(time.Second * 20)
	defer timer.Stop()

	for {
		select {
		case <-timer.C:
			peers := map[string]struct{}{}

			c.mu.RLock()
			for p, _ := range c.peers {
				peers[p] = struct{}{}
			}
			c.mu.RUnlock()
			for peer := range peers {
				fmt.Println("Peer: ", peer)

				peerUDPAddr, err := net.ResolveUDPAddr("udp", peer)
				if err != nil {
					fmt.Println(err)
					continue
				}

				currentTime := time.Now().String()

				msg := "Hello from peer " + currentTime
				message := utils.CreateMessageBytes(constants.PEER_MESSAGE, []byte(msg))
				c.udpConn.WriteTo(message, peerUDPAddr)
			}
		}
	}
}

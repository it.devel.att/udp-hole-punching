package entities

const (
	ActionSeedFile     = "ActionSeedFile"
	ActionPeerFile     = "ActionPeerFile"
	ActionDownloadFile = "ActionDownloadFile"
)

type Message struct {
	Action string
	Data   []byte
}

type SeedFile struct {
	FileName string
	FileHash string
}

type PeerFile struct {
	FileHash string
}

type DownloadFile struct {
	FileName string
	HexData  string
}

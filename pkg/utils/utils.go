package utils

import (
	"bytes"
	"encoding/binary"
)

func CreateMessageBytes(command int, v []byte) []byte {
	size := len(v)

	data := make([]byte, 0, 8+8+len(v))
	buff := bytes.NewBuffer(data)

	sizeBuf := new(bytes.Buffer)
	var num uint64 = uint64(size)
	binary.Write(sizeBuf, binary.BigEndian, num)

	commandBuff := new(bytes.Buffer)
	var commandOP uint64 = uint64(command)
	binary.Write(commandBuff, binary.BigEndian, commandOP)

	// First 8 it's command OP code
	buff.Write(commandBuff.Bytes())
	// Next 8 byte it's size of the message
	buff.Write(sizeBuf.Bytes())
	// message
	buff.Write(v)
	// commandOP code + messageLen + Message itself
	return buff.Bytes()
}

package constants

// Commands
const (
	_ = iota
	PING
	REGISTER
	GET_PEERS
	PEER_MESSAGE
)

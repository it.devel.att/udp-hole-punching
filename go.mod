module filesharetorrent

go 1.19

require github.com/pion/stun v0.4.0

require (
	github.com/pion/transport/v2 v2.0.0 // indirect
	golang.org/x/sys v0.2.0 // indirect
)
